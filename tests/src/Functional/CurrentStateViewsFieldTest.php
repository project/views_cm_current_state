<?php

declare(strict_types=1);

namespace Drupal\Tests\views_cm_current_state\Functional;

use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\views\Functional\ViewTestBase;

/**
 * Tests the views 'current_state_views_field' field plugin.
 *
 * @coversDefaultClass \Drupal\views_cm_current_state\Plugin\views\field\CurrentStateViewsField
 *
 * @group views_cm_current_state
 */
class CurrentStateViewsFieldTest extends ViewTestBase {

  use ContentModerationTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'content_moderation',
    'workflows',
    'views_cm_current_state',
    'views_cm_current_state_test_views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test views to import.
   */
  public static array $testViews = ['test_current_state_field'];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = []): void {
    parent::setUp(TRUE, ['views_cm_current_state_test_views']);

    $this->drupalCreateContentType([
      'type' => 'example_a',
    ]);

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'example_a');
    $workflow->save();

    $this->drupalLogin($this->drupalCreateUser([
      'access content',
    ]));

  }

  /**
   * Test that the field shows the current moderation state.
   */
  public function testCurrentViewsField(): void {
    // Create a published node.
    $node = $this->drupalCreateNode([
      'type' => 'example_a',
      'title' => 'Content 1',
      'moderation_state' => 'published',
    ]);

    // Create a draft revision.
    $node->set('moderation_state', 'draft');
    $node->save();

    $this->drupalGet('test-current-state-field');

    $assert_session = $this->assertSession();
    $assert_session->elementTextEquals('css', 'td.views-field-title', 'Content 1');
    // Core's moderation state field.
    $assert_session->elementTextEquals('css', 'td.views-field-moderation-state', 'Published');
    // This modules field.
    $assert_session->elementTextEquals('css', 'td.views-field-current-state-views-field', 'Draft');
  }

}
